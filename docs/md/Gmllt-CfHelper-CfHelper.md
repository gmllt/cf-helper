Gmllt\CfHelper\CfHelper
===============

Class CfHelper simplifies interaction with cloudfoundry environment variables.




* Class name: CfHelper
* Namespace: Gmllt\CfHelper
* This is an **abstract** class



Constants
----------


### CF_INSTANCE_ADDR

    const CF_INSTANCE_ADDR = "CF_INSTANCE_ADDR"





### CF_INSTANCE_GUID

    const CF_INSTANCE_GUID = "CF_INSTANCE_GUID"





### CF_INSTANCE_INDEX

    const CF_INSTANCE_INDEX = "CF_INSTANCE_INDEX"





### CF_INSTANCE_IP

    const CF_INSTANCE_IP = "CF_INSTANCE_IP"





### CF_INSTANCE_INTERNAL_IP

    const CF_INSTANCE_INTERNAL_IP = "CF_INSTANCE_INTERNAL_IP"





### CF_INSTANCE_PORT

    const CF_INSTANCE_PORT = "CF_INSTANCE_PORT"





### CF_INSTANCE_PORTS

    const CF_INSTANCE_PORTS = "CF_INSTANCE_PORTS"





### DATABASE_URL

    const DATABASE_URL = "DATABASE_URL"





### HOME

    const HOME = "HOME"





### LANG

    const LANG = "LANG"





### MEMORY_LIMIT

    const MEMORY_LIMIT = "MEMORY_LIMIT"





### PORT

    const PORT = "PORT"





### PWD

    const PWD = "PWD"





### TMPDIR

    const TMPDIR = "TMPDIR"





### USER

    const USER = "USER"





### VCAP_APP_PORT

    const VCAP_APP_PORT = "VCAP_APP_PORT"





### VCAP_APPLICATION

    const VCAP_APPLICATION = "VCAP_APPLICATION"





### VCAP_SERVICES

    const VCAP_SERVICES = "VCAP_SERVICES"





### VCAP_APPLICATION_ID

    const VCAP_APPLICATION_ID = "application_id"





### VCAP_APPLICATION_NAME

    const VCAP_APPLICATION_NAME = "application_name"





### VCAP_APPLICATION_URIS

    const VCAP_APPLICATION_URIS = "application_uris"





### VCAP_APPLICATION_VERSION

    const VCAP_APPLICATION_VERSION = "application_version"





### VCAP_APPLICATION_CF_API

    const VCAP_APPLICATION_CF_API = "cf_api"





### VCAP_APPLICATION_HOST

    const VCAP_APPLICATION_HOST = "host"





### VCAP_APPLICATION_LIMITS

    const VCAP_APPLICATION_LIMITS = "limits"





### VCAP_APPLICATION_SPACE_ID

    const VCAP_APPLICATION_SPACE_ID = "space_id"





### VCAP_APPLICATION_SPACE_NAME

    const VCAP_APPLICATION_SPACE_NAME = "space_name"





### VCAP_APPLICATION_START

    const VCAP_APPLICATION_START = "start"





### VCAP_APPLICATION_STARTED_AT

    const VCAP_APPLICATION_STARTED_AT = "started_at"





### VCAP_APPLICATION_STARTED_AT_TIMESTAMP

    const VCAP_APPLICATION_STARTED_AT_TIMESTAMP = "started_at_timestamp"





### VCAP_APPLICATION_STATE_TIMESTAMP

    const VCAP_APPLICATION_STATE_TIMESTAMP = "state_timestamp"





### VCAP_APPLICATION_USERS

    const VCAP_APPLICATION_USERS = "users"





### VCAP_SERVICES_NAME

    const VCAP_SERVICES_NAME = "name"





### VCAP_SERVICES_LABEL

    const VCAP_SERVICES_LABEL = "label"





### VCAP_SERVICES_TAGS

    const VCAP_SERVICES_TAGS = "tags"





### VCAP_SERVICES_PLAN

    const VCAP_SERVICES_PLAN = "plan"





### VCAP_SERVICES_CREDENTIALS

    const VCAP_SERVICES_CREDENTIALS = "credentials"







Methods
-------


### getEnv

    string Gmllt\CfHelper\CfHelper::getEnv(string $name)

Get environment variable by name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $name **string** - &lt;p&gt;environment variable name&lt;/p&gt;



### getVcapApplication

    mixed Gmllt\CfHelper\CfHelper::getVcapApplication(string $attribute)

Get VCAP_APPLICATION attribute by name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $attribute **string** - &lt;p&gt;attribute name&lt;/p&gt;



### getServiceInstances

    array Gmllt\CfHelper\CfHelper::getServiceInstances(string $brokerName)

Get services by service broker name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $brokerName **string** - &lt;p&gt;service broker name&lt;/p&gt;



### getService

    array Gmllt\CfHelper\CfHelper::getService(string $brokerName, string $instanceName)

Get service by instance name and service broker name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $brokerName **string** - &lt;p&gt;service broker name&lt;/p&gt;
* $instanceName **string** - &lt;p&gt;instance name&lt;/p&gt;



### getCredentials

    array Gmllt\CfHelper\CfHelper::getCredentials(string $brokerName, string $instanceName)

Get credentials by instance name and service broker name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $brokerName **string** - &lt;p&gt;service broker name&lt;/p&gt;
* $instanceName **string** - &lt;p&gt;service instance name&lt;/p&gt;



### get

    mixed Gmllt\CfHelper\CfHelper::get(string $brokerName, string $instanceName, string $credential)

Get credential by name, instance name and service broker name



* Visibility: **public**
* This method is **static**.


#### Arguments
* $brokerName **string** - &lt;p&gt;service broker name&lt;/p&gt;
* $instanceName **string** - &lt;p&gt;instance name&lt;/p&gt;
* $credential **string** - &lt;p&gt;credential name&lt;/p&gt;


